package ru.t1.ktitov.tm.command.user;

import ru.t1.ktitov.tm.enumerated.Role;
import ru.t1.ktitov.tm.util.TerminalUtil;

public final class UserChangePasswordCommand extends AbstractUserCommand {

    public static final String NAME = "user-change-password";

    public static final String DESCRIPTION = "Change password of current user";

    @Override
    public void execute() {
        System.out.println("[CHANGE PASSWORD]");
        System.out.print("ENTER NEW PASSWORD: ");
        final String userId = getAuthService().getUserId();
        final String password = TerminalUtil.nextLine();
        getUserService().setPassword(userId, password);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
