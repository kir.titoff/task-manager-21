package ru.t1.ktitov.tm.command.project;

import ru.t1.ktitov.tm.util.TerminalUtil;

public final class ProjectCreateCommand extends AbstractProjectCommand {

    public static final String NAME = "project-create";

    public static final String DESCRIPTION = "Create new project";

    @Override
    public void execute() {
        System.out.println("[CREATE PROJECT]");
        System.out.print("ENTER NAME: ");
        final String name = TerminalUtil.nextLine();
        System.out.print("ENTER DESCRIPTION: ");
        final String description = TerminalUtil.nextLine();
        final String userId = getUserId();
        getProjectService().create(userId, name, description);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
